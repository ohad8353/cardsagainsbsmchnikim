var express = require('express');
var socket = require('socket.io');

// game variables
let serialId = 0;
let room = [];
let judgeIndex = 0;
let roundFinished = false;
let answers = [];

// App setup
var app = express();
var server = app.listen(4000, function() {
	console.log('listening for requests on port 4000,');
});

// Static files
app.use(express.static('public'));

// Socket setup & pass server
var io = socket(server);

io.on('connection', socket => {
	console.log('made socket connection', socket.id);

	io.sockets.emit('connect');
	// Handle chat event
	socket.on('chat', function(data) {
		// console.log(data);
		io.sockets.emit('chat', data);
	});

	socket.on('updateSocket', function(loggedInUser) {
		const loggedUser = JSON.parse(loggedInUser);
		const loggedUserInRoom = room.find(user => user.name === loggedUser.name);
		if (loggedUserInRoom) {
			loggedUserInRoom.socket = socket;
			loggedUserInRoom.didPlay = false;
		} else {
			const newUser = {
				name: loggedUser.name,
				id: serialId,
				socket: socket,
				score: 0,
				didPlay: false
			};
			room = [...room, newUser];
			serialId++;
			socket.emit('loginResponse', JSON.stringify({ name: newUser.name, id: newUser.id }));
		}

		updateUsersList();
	});

	socket.on('disconnect', function(reason) {
		let userWhoLeft = room.find(user => user.socket === socket);

		try {
			if (userWhoLeft && userWhoLeft.id === room[judgeIndex].id) {
				roundFinished = true;
				nextSituation();
			}
			answers = answers.filter(answer => answer.playerId !== userWhoLeft.id);
			room = room.filter(user => user.socket !== socket);
			updateAnswers();
		} catch (err) {
			console.log(err);
		}

		updateUsersList();
	});

	socket.on('login', function(username) {
		const isUserExists = room.find(user => user.name === username);
		if (isUserExists) {
			socket.emit('userExists');
		} else {
			const newUser = {
				name: username,
				id: serialId,
				socket: socket,
				didPlay: false,
				score: 0
			};
			room = [...room, newUser];
			serialId++;
			socket.emit('loginResponse', JSON.stringify({ name: newUser.name, id: newUser.id }));

			updateUsersList();
		}
	});

	socket.on('exposeCardToJudge', function(data) {
		const playerId = JSON.parse(data).user.id;
		const card = JSON.parse(data).card;

		const user = room.find(user => user.id === playerId);
		if (user.id !== room[judgeIndex].id) {
			if (!user.didPlay) {
				user.didPlay = true;
				answers = [...answers, { answer: card, playerId: playerId }];
				socket.emit('switchCard', card);
				updateAnswers();
			} else {
				socket.emit('showPlayedAlready');
			}
		}
	});

	socket.on('chooseWinner', function(winnerCard) {
		const didAllPlay = room.filter(user => !user.didPlay).length <= 1;
		if (room[judgeIndex].socket === socket) {
			if (!didAllPlay) {
				socket.emit('waitForAll');
			} else {
				roundFinished = true;
				var winnerId = answers.find(answer => answer.answer === winnerCard).playerId;
				room.find(user => user.id === winnerId).score += 10;
				io.sockets.emit('declareWinner', winnerCard);
				updateUsersList();
			}
		}
	});

	socket.on('nextSituation', function() {
		nextSituation();
	});

	socket.on('resetGame', function() {
		io.sockets.emit('resetGame');
		room = [];
		answers = [];
	});

	function nextSituation(judgeLeft = false) {
		if (room[judgeIndex].socket === socket && roundFinished) {
			judgeIndex = (judgeIndex + judgeLeft ? 0 : 1) % room.length;
			io.sockets.emit('nextSituation', room[judgeIndex].name);
			roundFinished = true;
		}
	}
});
function updateAnswers() {
	room[judgeIndex].socket.emit(
		'updateCards',
		answers.map(answer => answer.answer)
	);
}

function updateUsersList() {
	io.sockets.emit(
		'usersListUpdated',
		JSON.stringify(
			room.map(user => {
				return { name: user.name, score: user.score };
			})
		)
	);
}
