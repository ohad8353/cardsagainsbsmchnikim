import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@mdi/font/css/materialdesignicons.css';
import VueSocketIO from 'vue-socket.io';

Vue.config.productionTip = false;

Vue.use(
	new VueSocketIO({
		debug: true,
		connection: 'localhost:4000'
	})
);

Vue.use(require('vue-shortkey'));

new Vue({
	router,
	store,
	vuetify,
	render: h => h(App)
}).$mount('#app');
